<?php
declare(strict_types=1);

namespace App\Infrastructure\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * @package Exception
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class ValidationException extends ValidatorException
{
    private ConstraintViolationListInterface $violations;
    
    public function __construct(ConstraintViolationListInterface $violations)
    {
        parent::__construct();
        $this->violations = $violations;
    }
    
    /**
     * Get the value of the violations property.
     *
     * @return ConstraintViolationListInterface
     */
    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
