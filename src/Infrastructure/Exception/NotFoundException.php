<?php
declare(strict_types=1);

namespace App\Infrastructure\Exception;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * @package Exception
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class NotFoundException extends Exception
{
    /**
     * @inheritDoc
     */
    public function __construct(string $resourceClass, ?Throwable $previous = null)
    {
        parent::__construct($resourceClass, Response::HTTP_NOT_FOUND, $previous);
    }
}
