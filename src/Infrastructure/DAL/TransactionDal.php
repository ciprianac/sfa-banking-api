<?php
declare(strict_types=1);

namespace App\Infrastructure\DAL;

use App\Infrastructure\DTO\Entity\TransactionDto;

/**
 * @package DAL
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionDal extends AbstractDal
{
    protected function getEntityClass(): string
    {
        return TransactionDto::class;
    }
    
    /**
     * @return TransactionDto[]
     */
    public function findByAccount(string $accountId): array
    {
        $query = $this->getEntityManager()->createQuery(
            "
                SELECT t
                FROM {$this->getEntityName()} t
                WHERE (t.sender = :accountId OR t.receiver = :accountId)
                ORDER BY t.id DESC
                "
        );
        
        return $query->setParameter('accountId', $accountId)
                     ->getResult();
    }
}
