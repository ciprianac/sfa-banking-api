<?php
declare(strict_types=1);

namespace App\Infrastructure\DAL;

use App\Infrastructure\Exception\NotFoundException;
use App\Infrastructure\Validator\Validator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @package DAL
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
abstract class AbstractDal extends ServiceEntityRepository
{
    public function __construct(readonly ManagerRegistry $managerRegistry, private readonly Validator $validator)
    {
        parent::__construct($managerRegistry, $this->getEntityClass());
    }
    
    abstract protected function getEntityClass(): string;
    
    /**
     * @throws NotFoundException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): object
    {
        $result = parent::findOneBy($criteria, $orderBy);
        if ($result === null) {
            throw new NotFoundException($this->getEntityClass());
        }
        
        return $result;
    }
    
    /**
     * @throws NotFoundException
     */
    public function findOneById(string $id): object
    {
        return $this->findOneBy(['id' => $id]);
    }
    
    /**
     * @return object[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }
    
    public function persist(object $instance): self
    {
        $this->validator->validate($instance);
        $this->getEntityManager()->persist($instance);
        
        return $this;
    }
    
    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}
