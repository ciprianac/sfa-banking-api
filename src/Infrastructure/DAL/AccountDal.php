<?php
declare(strict_types=1);

namespace App\Infrastructure\DAL;

use App\Infrastructure\DTO\Entity\AccountDto;

/**
 * @package DAL
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountDal extends AbstractDal
{
    protected function getEntityClass(): string
    {
        return AccountDto::class;
    }
    
    public function findAllOrderedByBalanceDescending(): array
    {
        $query = $this->getEntityManager()->createQuery(
            "
                SELECT a
                FROM {$this->getEntityName()} a
                ORDER BY a.balance DESC
                "
        );
    
        return $query->getResult();
    }
    
    public function findWithNegativeBalance(): array
    {
        $query = $this->getEntityManager()->createQuery(
            "
                SELECT a
                FROM {$this->getEntityName()} a
                WHERE a.balance < 0
                "
        );
        
        return $query->getResult();
    }
}
