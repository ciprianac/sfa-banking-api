<?php
declare(strict_types=1);

namespace App\Infrastructure\DTO\Entity;

use App\Infrastructure\DAL\TransactionDal;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @package DTO\Entity
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[ORM\Entity(repositoryClass: TransactionDal::class)]
#[ORM\Table(name: 'transaction')]
#[ORM\Index(columns: ['sender_id', 'receiver_id'], name: 'transaction_index')]
class TransactionDto extends AbstractEntity
{
    #[ORM\ManyToOne(targetEntity: AccountDto::class)]
    #[Assert\NotBlank]
    #[Assert\Type((AccountDto::class))]
    #[Assert\Valid]
    private AccountDto $sender;
    
    #[ORM\ManyToOne(targetEntity: AccountDto::class)]
    #[Assert\NotBlank]
    #[Assert\Type((AccountDto::class))]
    #[Assert\Valid]
    private AccountDto $receiver;
    
    #[ORM\Column(name: 'amount', type: 'float')]
    #[Assert\NotBlank]
    #[Assert\Type(type: 'float')]
    private float $amount;
    
    public function __construct(AccountDto $sender, AccountDto $receiver, float $amount)
    {
        $this->sender   = $sender;
        $this->receiver = $receiver;
        $this->amount   = $amount;
    }
    
    public function getSender(): AccountDto
    {
        return $this->sender;
    }
    
    public function setSender(AccountDto $sender): self
    {
        $this->sender = $sender;
        
        return $this;
    }
    
    public function getReceiver(): AccountDto
    {
        return $this->receiver;
    }
    
    public function setReceiver(AccountDto $receiver): self
    {
        $this->receiver = $receiver;
        
        return $this;
    }
    
    public function getAmount(): float
    {
        return $this->amount;
    }
    
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        
        return $this;
    }
}
