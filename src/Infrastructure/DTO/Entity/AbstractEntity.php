<?php
declare(strict_types=1);

namespace App\Infrastructure\DTO\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

/**
 * @package DTO
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[ORM\MappedSuperclass]
#[ORM\HasLifecycleCallbacks]
abstract class AbstractEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'string', length: 26, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UlidGenerator::class)]
    private ?string $id = null;
    
    #[ORM\Column(name: "created_at", type: "datetime_immutable")]
    private ?DateTimeImmutable $createdAt = null;
    
    #[ORM\Column(name: "updated_at", type: "datetime_immutable")]
    private ?DateTimeImmutable $updatedAt = null;
    
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
    
    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }
    
    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $currentTime = new DateTimeImmutable();
        
        if (is_null($this->createdAt)) {
            $this->createdAt = $currentTime;
        }
        
        if (is_null($this->updatedAt)) {
            $this->updatedAt = $currentTime;
        }
    }
    
    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $currentTime     = new DateTimeImmutable();
        $this->updatedAt = $currentTime;
    }
}
