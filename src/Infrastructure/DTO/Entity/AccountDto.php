<?php
declare(strict_types=1);

namespace App\Infrastructure\DTO\Entity;

use App\Infrastructure\DAL\AccountDal;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @package DTO\Entity
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[ORM\Entity(repositoryClass: AccountDal::class)]
#[ORM\Table(name: 'account')]
class AccountDto extends AbstractEntity
{
    #[ORM\Column(name: 'first_name', type: 'string', length: 120)]
    #[Assert\NotBlank]
    #[Assert\Type(type: 'string')]
    #[Assert\Length(min: 3, max: 120)]
    private string $firstName;
    
    #[ORM\Column(name: 'last_name', type: 'string', length: 120)]
    #[Assert\NotBlank]
    #[Assert\Type(type: 'string')]
    #[Assert\Length(min: 3, max: 120)]
    private string $lastName;
    
    #[ORM\Column(name: 'balance', type: 'float')]
    #[Assert\NotBlank]
    #[Assert\Type(type: 'float')]
    private float $balance;
    
    #[ORM\Column(name: 'overdraft', type: 'float')]
    #[Assert\NotBlank]
    #[Assert\Type(type: 'float')]
    private float $overdraft;
    
    public function __construct(string $firstName, string $lastName, float $balance, float $overdraft)
    {
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
        $this->balance   = $balance;
        $this->overdraft = $overdraft;
    }
    
    
    public function getFirstName(): string
    {
        return $this->firstName;
    }
    
    public function getLastName(): string
    {
        return $this->lastName;
    }
    
    public function getBalance(): float
    {
        return $this->balance;
    }
    
    public function setBalance(float $balance): self
    {
        $this->balance = $balance;
        
        return $this;
    }
    
    public function getOverdraft(): float
    {
        return $this->overdraft;
    }
}
