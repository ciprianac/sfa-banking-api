<?php
declare(strict_types=1);

namespace App\Infrastructure\DTO\Message;

/**
 * Class TransactionCreateMessage.
 *
 * @package App\Infrastructure\DTO\Message
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionCreateMessage
{
    private string $senderId;
    
    private string $receiverId;
    
    private float $amount;
    
    
    public function __construct(string $senderId, string $receiverId, float $amount)
    {
        $this->senderId   = $senderId;
        $this->receiverId = $receiverId;
        $this->amount     = $amount;
    }
    
    public function getSenderId(): string
    {
        return $this->senderId;
    }
    
    public function getReceiverId(): string
    {
        return $this->receiverId;
    }
    
    public function getAmount(): float
    {
        return $this->amount;
    }
}
