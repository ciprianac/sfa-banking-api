<?php
declare(strict_types=1);

namespace App\Infrastructure\Validator\Validator;

use App\Application\Command\TransactionCreateCommand;
use App\Application\Service\TransactionValidityService;
use App\Infrastructure\Validator\Constraint\AccountSufficientFunds;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountSufficientFundsValidator extends ConstraintValidator
{
    
    public function __construct(private readonly TransactionValidityService $transactionValidityService)
    {
    }
    
    /**
     * @param TransactionCreateCommand $value
     * @param Constraint               $constraint
     *
     * @return void
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if ( ! $constraint instanceof AccountSufficientFunds) {
            return;
        }
        
        $hasSufficientFunds = $this->transactionValidityService->checkValidity(
            $value->getSenderId(),
            $value->getAmount()
        );
        if ($hasSufficientFunds === false) {
            $this->context->buildViolation($constraint->message)
                          ->addViolation();
        }
    }
    
}
