<?php
declare(strict_types=1);

namespace App\Infrastructure\Validator\Validator;

use App\Application\Command\TransactionCreateCommand;
use App\Infrastructure\Validator\Constraint\DifferentAccounts;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class DifferentAccountsValidator extends ConstraintValidator
{
    /**
     * @param TransactionCreateCommand $value
     * @param Constraint               $constraint
     *
     * @return void
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if ( ! $constraint instanceof DifferentAccounts) {
            return;
        }
        
        if ($value->getSenderId() === $value->getReceiverId()) {
            $this->context->buildViolation($constraint->message)
                          ->addViolation();
        }
    }
    
}
