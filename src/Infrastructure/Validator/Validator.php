<?php
declare(strict_types=1);

namespace App\Infrastructure\Validator;

use App\Infrastructure\Exception\ValidationException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Validator.
 *
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class Validator
{
    public function __construct(
        private readonly ValidatorInterface $validator,
    ) {
    }
    
    /**
     * Validate an instance.
     *
     * @param mixed                        $instance
     * @param Constraint|Constraint[]|null $constraints
     * @param array|null                   $validationGroups
     *
     * @return bool
     * @throws ValidationException
     */
    public function validate(
        mixed $instance,
        array|Constraint $constraints = null,
        array $validationGroups = null
    ): bool {
        $validationErrors = $this->validator->validate($instance, $constraints, $validationGroups);
        
        if (0 < $validationErrors->count()) {
            throw new ValidationException($validationErrors);
        }
        
        return true;
    }
}
