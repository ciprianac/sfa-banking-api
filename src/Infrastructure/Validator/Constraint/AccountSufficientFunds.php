<?php
declare(strict_types=1);

namespace App\Infrastructure\Validator\Constraint;

use App\Infrastructure\Validator\Validator\AccountSufficientFundsValidator;
use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Attribute]
class AccountSufficientFunds extends Constraint
{
    public string $message = "Insufficient funds for this transaction";
    
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy(): string
    {
        return AccountSufficientFundsValidator::class;
    }
}
