<?php
declare(strict_types=1);

namespace App\Infrastructure\Validator\Constraint;

use App\Infrastructure\Validator\Validator\AccountSufficientFundsValidator;
use App\Infrastructure\Validator\Validator\DifferentAccountsValidator;
use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * Class DifferentAccounts.
 *
 * @package App\Infrastructure\Validator\Constraint
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Attribute]
class DifferentAccounts extends Constraint
{
    public string $message = "Sender and receiver accounts must be different";
    
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function validatedBy(): string
    {
        return DifferentAccountsValidator::class;
    }
}
