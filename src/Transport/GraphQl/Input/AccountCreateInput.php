<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Input;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @package App\GraphQl\Input
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Input]
class AccountCreateInput
{
    #[Field]
    #[Assert\NotBlank]
    #[Assert\Length(max: 120)]
    #[Assert\Type('string')]
    private readonly string $firstName;
    
    #[Field]
    #[Assert\NotBlank]
    #[Assert\Length(max: 120)]
    #[Assert\Type('string')]
    private readonly string $lastName;
    
    #[Field]
    #[Assert\NotBlank]
    #[Assert\Type('float')]
    private readonly float $balance;
    
    public function __construct(string $firstName, string $lastName, float $balance)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->balance  = $balance;
    }
    
    public function getFirstName(): string
    {
        return $this->firstName;
    }
    
    public function getLastName(): string
    {
        return $this->lastName;
    }
    
    public function getBalance(): float
    {
        return $this->balance;
    }
    
}
