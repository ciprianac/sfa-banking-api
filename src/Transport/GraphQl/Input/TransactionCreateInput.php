<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Input;

use App\Infrastructure\Validator\Constraint\AccountSufficientFunds;
use App\Infrastructure\Validator\Constraint\DifferentAccounts;
use Symfony\Component\Validator\Constraints as Assert;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Input;

/**
 * Class TransactionCreateInput.
 *
 * @package App\GraphQl\Input
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Input]
class TransactionCreateInput
{
    #[Field]
    #[Assert\Type('string')]
    #[Assert\Length(exactly: 26)]
    #[Assert\NotBlank]
    #[Assert\Ulid]
    private readonly string $senderId;
    
    #[Field]
    #[Assert\Type('string')]
    #[Assert\Length(exactly: 26)]
    #[Assert\NotBlank]
    #[Assert\Ulid]
    private readonly string $receiverId;
    
    #[Field]
    #[Assert\Type('float')]
    #[Assert\NotBlank]
    private readonly float $amount;
    
    public function __construct(string $senderId, string $receiverId, float $amount)
    {
        $this->senderId   = $senderId;
        $this->receiverId = $receiverId;
        $this->amount     = $amount;
    }
    
    public function getSenderId(): string
    {
        return $this->senderId;
    }
    
    public function getReceiverId(): string
    {
        return $this->receiverId;
    }
    
    public function getAmount(): float
    {
        return $this->amount;
    }
    
}
