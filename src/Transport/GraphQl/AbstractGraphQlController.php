<?php
declare(strict_types=1);

namespace App\Transport\GraphQl;

use App\Infrastructure\Validator\Validator;
use League\Tactician\CommandBus;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @package App\GraphQl
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
abstract class AbstractGraphQlController
{
    public function __construct(
        protected readonly CommandBus          $commandBus,
        protected readonly CommandBus          $queryBus,
        protected readonly MessageBusInterface $messageBus,
        protected readonly Validator           $validator
    ) {
    }
}
