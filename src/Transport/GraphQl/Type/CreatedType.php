<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Type;

use Symfony\Component\HttpFoundation\Response;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @package App\Transport\GraphQl\Type
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Type(name: 'Created')]
class CreatedType
{
    #[Field]
    private int $code;
    
    public function __construct(int $code = Response::HTTP_OK)
    {
        $this->code = $code;
    }
    
    /**
     * Get the value of the code property.
     *
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }
}
