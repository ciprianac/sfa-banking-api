<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Type;

use App\Infrastructure\DTO\Entity\TransactionDto;
use TheCodingMachine\GraphQLite\Annotations\SourceField;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @package App\GraphQl\Type
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Type(class: TransactionDto::class, name: 'Transaction')]
#[SourceField(name: 'id')]
#[SourceField(name: 'createdAt')]
#[SourceField(name: 'updatedAt')]
#[SourceField(name: 'sender')]
#[SourceField(name: 'receiver')]
#[SourceField(name: 'amount')]
class TransactionType
{
    
}
