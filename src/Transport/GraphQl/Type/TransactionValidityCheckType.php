<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Type;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @package App\Transport\GraphQl\Type
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Type(name: 'TransactionValidityCheck')]
class TransactionValidityCheckType
{
    #[Field]
    private bool $canPerformTransaction;
    
    public function __construct(bool $canPerformTransaction)
    {
        $this->canPerformTransaction = $canPerformTransaction;
    }
    
    public function isCanPerformTransaction(): bool
    {
        return $this->canPerformTransaction;
    }
}
