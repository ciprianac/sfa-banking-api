<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Type;

use App\Infrastructure\DTO\Entity\AccountDto;
use TheCodingMachine\GraphQLite\Annotations\SourceField;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @package App\GraphQl\Type
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[Type(class: AccountDto::class, name: 'Account')]
#[SourceField(name: 'id')]
#[SourceField(name: 'createdAt')]
#[SourceField(name: 'updatedAt')]
#[SourceField(name: 'firstName')]
#[SourceField(name: 'lastName')]
#[SourceField(name: 'balance')]
#[SourceField(name: 'overdraft')]
class AccountType
{
    
}
