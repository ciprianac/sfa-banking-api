<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Controller\Query;

use App\Application\Query\TransactionVerifyFundsQuery;
use App\Transport\GraphQl\AbstractGraphQlController;
use App\Transport\GraphQl\Type\TransactionValidityCheckType;
use TheCodingMachine\GraphQLite\Annotations\Query;

/**
 * @package App\Transport\GraphQl\Controller\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionValidityCheckController extends AbstractGraphQlController
{
    #[Query(name: 'transactionValidity')]
    public function __invoke(string $senderId, float $amount): TransactionValidityCheckType
    {
        return new TransactionValidityCheckType(
            $this->queryBus->handle(new TransactionVerifyFundsQuery($senderId, $amount))
        );
    }
}
