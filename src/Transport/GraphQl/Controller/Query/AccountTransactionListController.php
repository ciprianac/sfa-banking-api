<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Controller\Query;

use App\Application\Query\AccountTransactionListQuery;
use App\Infrastructure\DTO\Entity\TransactionDto;
use App\Transport\GraphQl\AbstractGraphQlController;
use TheCodingMachine\GraphQLite\Annotations\Query;

/**
 * Class AccountTransactionListController.
 *
 * @package App\Transport\GraphQl\Controller\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountTransactionListController extends AbstractGraphQlController
{
    /** @return TransactionDto[] */
    #[Query(name: 'listAccountTransactions')]
    public function __invoke(string $accountId): array
    {
        return $this->queryBus->handle(new AccountTransactionListQuery($accountId));
    }
}
