<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Controller\Query;

use App\Application\Query\AccountNegativeBalanceQuery;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Transport\GraphQl\AbstractGraphQlController;
use TheCodingMachine\GraphQLite\Annotations\Query;

/**
 * Class AccountNegativeBalanceController.
 *
 * @package App\GraphQl\Controller\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountNegativeBalanceController extends AbstractGraphQlController
{
    /** @return AccountDto[] */
    #[Query(name: 'negativeBalance')]
    public function __invoke(): array
    {
        return $this->queryBus->handle(new AccountNegativeBalanceQuery());
    }
}
