<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Controller\Query;

use App\Application\Query\AccountListQuery;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Transport\GraphQl\AbstractGraphQlController;
use TheCodingMachine\GraphQLite\Annotations\Query;

/**
 * @package App\GraphQl\Controller\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountListController extends AbstractGraphQlController
{
    /** @return AccountDto[] */
    #[Query(name: 'listAccounts')]
    public function __invoke(): array
    {
        return $this->queryBus->handle(new AccountListQuery());
    }
}
