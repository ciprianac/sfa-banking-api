<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Controller\Mutation;

use App\Infrastructure\DTO\Message\TransactionCreateMessage;
use App\Transport\GraphQl\AbstractGraphQlController;
use App\Transport\GraphQl\Input\TransactionCreateInput;
use App\Transport\GraphQl\Type\CreatedType;
use TheCodingMachine\GraphQLite\Annotations\Mutation;

/**
 * @package App\GraphQl\Controller\Mutation
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionCreateController extends AbstractGraphQlController
{
    #[Mutation(name: 'performTransaction')]
    public function __invoke(TransactionCreateInput $input): CreatedType
    {
        $this->validator->validate($input);
        $message = new TransactionCreateMessage(
            $input->getSenderId(),
            $input->getReceiverId(),
            $input->getAmount()
        );
        $this->messageBus->dispatch($message);
        
        return new CreatedType();
    }
}
