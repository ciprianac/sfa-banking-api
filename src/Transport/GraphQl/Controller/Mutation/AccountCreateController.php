<?php
declare(strict_types=1);

namespace App\Transport\GraphQl\Controller\Mutation;

use App\Application\Command\AccountCreateCommand;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Transport\GraphQl\AbstractGraphQlController;
use App\Transport\GraphQl\Input\AccountCreateInput;
use TheCodingMachine\GraphQLite\Annotations\Mutation;

/**
 * @package App\GraphQl\Mutation
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountCreateController extends AbstractGraphQlController
{
    #[Mutation(name: 'openAccount')]
    public function __invoke(AccountCreateInput $input): AccountDto
    {
        $this->validator->validate($input);
        
        return $this->commandBus->handle(
            new AccountCreateCommand(
                firstName: $input->getFirstName(),
                lastName: $input->getLastName(),
                balance: $input->getBalance()
            )
        );
    }
}
