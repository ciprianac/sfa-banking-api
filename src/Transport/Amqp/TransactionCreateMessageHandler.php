<?php
declare(strict_types=1);

namespace App\Transport\Amqp;

use App\Application\Command\TransactionCreateCommand;
use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Infrastructure\DTO\Message\TransactionCreateMessage;
use App\Infrastructure\Exception\NotFoundException;
use League\Tactician\CommandBus;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

/**
 * @package App\Transport\Amqp
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[AsMessageHandler]
class TransactionCreateMessageHandler
{
    public function __construct(
        private readonly CommandBus $commandBus,
        private readonly AccountDal $accountDal
    ) {
    }
    
    /**
     * @throws NotFoundException
     */
    public function __invoke(TransactionCreateMessage $message): void
    {
        $this->commandBus->handle(
            new TransactionCreateCommand($message->getSenderId(), $message->getReceiverId(), $message->getAmount())
        );
        
        /** @var AccountDto $sender */
        $sender = $this->accountDal->findOneById($message->getSenderId());
        $sender->setBalance($sender->getBalance() - $message->getAmount());
        
        
        /** @var AccountDto $sender */
        $receiver = $this->accountDal->findOneById($message->getReceiverId());
        $receiver->setBalance($receiver->getBalance() + $message->getAmount());
        $this->accountDal->persist($receiver)
                         ->persist($sender);
    }
}
