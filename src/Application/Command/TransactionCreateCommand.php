<?php
declare(strict_types=1);

namespace App\Application\Command;

use App\Infrastructure\Validator\Constraint\AccountSufficientFunds;
use App\Infrastructure\Validator\Constraint\DifferentAccounts;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
#[AccountSufficientFunds]
#[DifferentAccounts]
class TransactionCreateCommand
{
    #[Assert\Length(exactly: 26)]
    #[Assert\NotBlank]
    #[Assert\Ulid]
    private readonly string $senderId;
    
    #[Assert\Length(exactly: 26)]
    #[Assert\NotBlank]
    #[Assert\Ulid]
    private readonly string $receiverId;
    
    #[Assert\Type('float')]
    #[Assert\NotBlank]
    #[Assert\Positive]
    private readonly float $amount;
    
    public function __construct(string $senderId, string $receiverId, float $amount)
    {
        $this->senderId   = $senderId;
        $this->receiverId = $receiverId;
        $this->amount     = $amount;
    }
    
    public function getSenderId(): string
    {
        return $this->senderId;
    }
    
    public function getReceiverId(): string
    {
        return $this->receiverId;
    }
    
    public function getAmount(): float
    {
        return $this->amount;
    }
}
