<?php
declare(strict_types=1);

namespace App\Application\Command;

use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountCreateCommandHandler
{
    public function __construct(
        private readonly AccountDal $accountDal,
        private readonly float      $overdraft
    ) {
    }
    
    public function __invoke(AccountCreateCommand $command): AccountDto
    {
        $account = new AccountDto(
            $command->getFirstName(),
            $command->getLastName(),
            $command->getBalance(),
            $this->overdraft
        );
        $this->accountDal->persist($account);
        
        return $account;
    }
    
}
