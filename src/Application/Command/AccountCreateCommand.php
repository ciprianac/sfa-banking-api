<?php
declare(strict_types=1);

namespace App\Application\Command;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountCreateCommand
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 120)]
    #[Assert\Type('string')]
    private readonly string $firstName;
    
    #[Assert\NotBlank]
    #[Assert\Length(max: 120)]
    #[Assert\Type('string')]
    private readonly string $lastName;
    
    #[Assert\NotBlank]
    #[Assert\PositiveOrZero]
    #[Assert\Type('float')]
    private readonly float $balance;
    
    /**
     * AccountCreateCommand constructor.
     *
     * @param string $firstName
     * @param string $lastName
     * @param float  $balance
     */
    public function __construct(string $firstName, string $lastName, float $balance)
    {
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
        $this->balance   = $balance;
    }
    
    public function getFirstName(): string
    {
        return $this->firstName;
    }
    
    public function getLastName(): string
    {
        return $this->lastName;
    }
    
    public function getBalance(): float
    {
        return $this->balance;
    }
}
