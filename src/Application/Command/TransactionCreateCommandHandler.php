<?php
declare(strict_types=1);

namespace App\Application\Command;

use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DAL\TransactionDal;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Infrastructure\DTO\Entity\TransactionDto;
use App\Infrastructure\Exception\NotFoundException;

/**
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionCreateCommandHandler
{
    public function __construct(
        private readonly TransactionDal $transactionDal,
        private readonly AccountDal     $accountDal
    ) {
    }
    
    /**
     * @throws NotFoundException
     */
    public function __invoke(TransactionCreateCommand $command): TransactionDto
    {
        /** @var AccountDto $sender */
        $sender = $this->accountDal->findOneById($command->getSenderId());
        /** @var AccountDto $receiver */
        $receiver = $this->accountDal->findOneById($command->getReceiverId());
        
        $transaction = new TransactionDto($sender, $receiver, $command->getAmount());
        $this->transactionDal->persist($transaction);
        
        return $transaction;
    }
}
