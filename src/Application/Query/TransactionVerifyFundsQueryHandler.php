<?php
declare(strict_types=1);

namespace App\Application\Query;

use App\Application\Service\TransactionValidityService;

/**
 * Responsible for defining the transaction verify funds handler command.
 *
 * @package App\Application\Command
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionVerifyFundsQueryHandler
{
    public function __construct(private readonly TransactionValidityService $transactionValidityService)
    {
    }
    
    public function __invoke(TransactionVerifyFundsQuery $command): bool
    {
        return $this->transactionValidityService->checkValidity($command->getSenderId(), $command->getAmount());
    }
    
}
