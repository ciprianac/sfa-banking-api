<?php
declare(strict_types=1);

namespace App\Application\Query;

use App\Infrastructure\DAL\AccountDal;

/**
 * @package App\Application\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountNegativeBalanceQueryHandler
{
    
    public function __construct(private readonly AccountDal $accountDal)
    {
    }

    public function __invoke(AccountNegativeBalanceQuery $query): array
    {
        return $this->accountDal->findWithNegativeBalance();
    }
    
}
