<?php
declare(strict_types=1);

namespace App\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @package App\Application\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountTransactionListQuery
{
    #[Assert\Length(exactly: 26)]
    #[Assert\Ulid]
    #[Assert\NotBlank]
    private readonly string $accountId;
    
    public function __construct(string $accountId)
    {
        $this->accountId = $accountId;
    }
    
    public function getAccountId(): string
    {
        return $this->accountId;
    }
}
