<?php
declare(strict_types=1);

namespace App\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @package App\Application\Command
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionVerifyFundsQuery
{
    #[Assert\Type('string')]
    #[Assert\Length(exactly: 26)]
    #[Assert\NotBlank]
    #[Assert\Ulid]
    private readonly string $senderId;
    
    #[Assert\Type('float')]
    #[Assert\NotBlank]
    #[Assert\Positive]
    private readonly float $amount;
    
    public function __construct(string $senderId, float $amount)
    {
        $this->senderId = $senderId;
        $this->amount   = $amount;
    }
    
    public function getSenderId(): string
    {
        return $this->senderId;
    }
    
    public function getAmount(): float
    {
        return $this->amount;
    }
    
}
