<?php
declare(strict_types=1);

namespace App\Application\Query;

use App\Infrastructure\DAL\TransactionDal;

/**
 * @package App\Application\Query
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountTransactionListQueryHandler
{
    public function __construct(private readonly TransactionDal $transactionDal)
    {
    }
    
    public function __invoke(AccountTransactionListQuery $query): array
    {
        return $this->transactionDal->findByAccount($query->getAccountId());
    }
}
