<?php
declare(strict_types=1);

namespace App\Application\Service;

use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;

/**
 * Class TransactionValidityService.
 *
 * @package App\Application\Service
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionValidityService
{
    public function __construct(private readonly AccountDal $accountDal)
    {
    }
    
    public function checkValidity(string $senderId, float $amount): bool
    {
        /** @var AccountDto $senderAccount */
        $senderAccount = $this->accountDal->findOneById($senderId);
        
        return ($senderAccount->getBalance() + $senderAccount->getOverdraft() - $amount) >= 0;
    }
}
