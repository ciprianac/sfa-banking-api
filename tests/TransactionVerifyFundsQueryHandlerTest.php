<?php
declare(strict_types=1);

namespace App\Tests;

use App\Application\Query\TransactionVerifyFundsQuery;
use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;
use Throwable;

/**
 * Test the behaviour of the transaction verify funds query handler class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionVerifyFundsQueryHandlerTest extends AbstractTest
{
    private const POSITIVE_VALUE_MESSAGE = 'This value should be positive.';
    private const VALID_ULID_MESSAGE     = 'This is not a valid ULID.';
    private const STRING_LENGTH_MESSAGE  = 'This value should have exactly 26 characters.';
    protected AccountDal $accountDal;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->accountDal = self::getContainer()->get(AccountDal::class);
    }
    
    protected function tearDown(): void
    {
        unset($this->accountDal);
        parent::tearDown();
    }
    
    public function testSufficientFunds()
    {
        $sender = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            50,
            100
        );
        
        $this->accountDal->persist($sender)
                         ->flush();
        $result = $this->queryBus->handle(
            new TransactionVerifyFundsQuery(
                $sender->getId(),
                100
            )
        );
        
        self::assertEquals(true, $result);
        self::assertIsBool($result);
    }
    
    public function testInsufficientFunds()
    {
        $sender = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            50,
            100
        );
        
        $this->accountDal->persist($sender)
                         ->flush();
        $result = $this->queryBus->handle(
            new TransactionVerifyFundsQuery(
                $sender->getId(),
                300
            )
        );
        
        self::assertEquals(false, $result);
        self::assertIsBool($result);
    }
    
    public function testWithNegativeAmount()
    {
        $sender = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            50,
            100
        );
        
        $this->accountDal->persist($sender)
                         ->flush();
        try {
            $this->queryBus->handle(
                new TransactionVerifyFundsQuery(
                    $sender->getId(),
                    -122
                )
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::POSITIVE_VALUE_MESSAGE);
        }
    }
    
    public function testWithInvalidUlid()
    {
        try {
            $this->queryBus->handle(
                new TransactionVerifyFundsQuery(
                    $this->faker->text(10),
                    100
                )
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 4);
            foreach ($exception->getViolations() as $violation){
                self::assertTrue(
                    $violation->getMessage() === self::VALID_ULID_MESSAGE ||
                    $violation->getMessage() === self::STRING_LENGTH_MESSAGE
                );
            }
        }
    }
}
