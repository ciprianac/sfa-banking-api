<?php
declare(strict_types=1);

namespace App\Tests;

use App\Application\Command\TransactionCreateCommand;
use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Infrastructure\DTO\Entity\TransactionDto;

/**
 * Test the behaviour of the transaction create command handler class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class TransactionCreateCommandHandlerTest extends AbstractTest
{
    private const DIFFERENT_ACCOUNTS_MESSAGE = 'Sender and receiver accounts must be different';
    private const INSUFFICIENT_FUNDS_MESSAGE = 'Insufficient funds for this transaction';
    private const POSITIVE_VALUE_MESSAGE     = 'This value should be positive.';
    protected AccountDal $accountDal;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->accountDal = self::getContainer()->get(AccountDal::class);
    }
    
    protected function tearDown(): void
    {
        unset($this->accountDal);
        parent::tearDown();
    }
    
    public function testCreateTransaction()
    {
        $sender = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(2, 100, 200),
            $this->faker->randomFloat()
        );
        
        $receiver = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        $this->accountDal->persist($sender)
                         ->persist($receiver)
                         ->flush();
        
        $expectedAmount = $this->faker->randomFloat(2, 1, 100);
        $result         = $this->commandBus->handle(
            new TransactionCreateCommand(
                $sender->getId(),
                $receiver->getId(),
                $expectedAmount
            )
        );
        
        self::assertInstanceOf(TransactionDto::class, $result);
        self::assertEquals($expectedAmount, $result->getAmount());
        self::assertEquals($sender, $result->getSender());
        self::assertEquals($receiver, $result->getReceiver());
    }
    
    public function testCreateWithSameSenderAndReceiver()
    {
        $account = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(2, 1000, 2000),
            $this->faker->randomFloat()
        );
        $this->accountDal->persist($account)
                         ->flush();
        
        try {
            $this->commandBus->handle(
                new TransactionCreateCommand(
                    $account->getId(),
                    $account->getId(),
                    $this->faker->randomFloat(2, 100, 200),
                )
            );
        } catch (\Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::DIFFERENT_ACCOUNTS_MESSAGE);
        }
    }
    
    public function testCreateWithoutEnoughFounds()
    {
        $sender = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(2, 100, 200),
            $this->faker->randomFloat(2, 50, 100)
        );
        
        $receiver = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        $this->accountDal->persist($sender)
                         ->persist($receiver)
                         ->flush();
        
        try {
            $this->commandBus->handle(
                new TransactionCreateCommand(
                    $sender->getId(),
                    $receiver->getId(),
                    $this->faker->randomFloat(2, 500, 600),
                )
            );
        } catch (\Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::INSUFFICIENT_FUNDS_MESSAGE);
        }
    }
    
    public function testCreateWithNegativeAmount()
    {
        $sender = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(2, 100, 200),
            $this->faker->randomFloat(2, 50, 100)
        );
        
        $receiver = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        $this->accountDal->persist($sender)
                         ->persist($receiver)
                         ->flush();
        
        try {
            $this->commandBus->handle(
                new TransactionCreateCommand(
                    $sender->getId(),
                    $receiver->getId(),
                    -100,
                )
            );
        } catch (\Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::POSITIVE_VALUE_MESSAGE);
        }
    }
}
