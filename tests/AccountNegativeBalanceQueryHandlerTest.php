<?php
declare(strict_types=1);

namespace App\Tests;

use App\Application\Query\AccountNegativeBalanceQuery;
use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;

/**
 * Test the behaviour of the account negative balance query handler class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountNegativeBalanceQueryHandlerTest extends AbstractTest
{
    protected AccountDal $accountDal;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->accountDal = self::getContainer()->get(AccountDal::class);
    }
    
    protected function tearDown(): void
    {
        unset($this->accountDal);
        parent::tearDown();
    }
    
    public function testNegativeBalance()
    {
        $account1 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            -50,
            100
        );
        
        $account2 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            100,
            100
        );
        $this->accountDal->persist($account1)
                         ->persist($account2)
                         ->flush();
        
        $result = $this->queryBus->handle(new AccountNegativeBalanceQuery());
        self::assertIsArray($result);
        
        /** @var AccountDto $account */
        foreach ($result as $account){
            self::assertInstanceOf(AccountDto::class, $account);
            self::assertLessThan(0,$account->getBalance());
        }
    }
}
