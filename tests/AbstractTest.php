<?php
declare(strict_types=1);

namespace App\Tests;

use Faker\Generator;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test the behaviour of the abstract handler class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
abstract class AbstractTest extends KernelTestCase
{
    protected CommandBus $queryBus;
    protected CommandBus $commandBus;
    protected Generator $faker;
    
    protected function setUp(): void
    {
        if (static::$booted === false) {
            static::bootKernel($this->getKernelOptions());
        }
        $this->queryBus     = static::getContainer()->get('tactician.commandbus.query');
        $this->commandBus   = static::getContainer()->get('tactician.commandbus.command');
        $this->faker        = static::getContainer()->get(Generator::class);
        $this->faker->seed();
        
        parent::setUp();
    }
    
    protected function getKernelOptions(): array
    {
        return [];
    }
    
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->queryBus);
        unset($this->commandBus);
        unset($this->userCache);
        unset($this->profileDal);
        unset($this->tokenStorage);
        unset($this->faker);
    }
}
