<?php
declare(strict_types=1);

namespace App\Tests;

use App\Application\Query\AccountListQuery;
use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DTO\Entity\AccountDto;

/**
 * Test the behaviour of the account list query handler class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountListQueryHandlerTest extends AbstractTest
{
    protected AccountDal $accountDal;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->accountDal = self::getContainer()->get(AccountDal::class);
    }
    
    protected function tearDown(): void
    {
        unset($this->accountDal);
        parent::tearDown();
    }
    
    public function testAccountListOrderedByBalanceDescending()
    {
        $account1 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        
        $account2 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        $this->accountDal->persist($account1)
                         ->persist($account2)
                         ->flush();
        
        $result      = $this->queryBus->handle(new AccountListQuery());
        $sortedArray = $result;
        usort($sortedArray, function ($a, $b) {
            return $a->getBalance() < $b->getBalance() ? 1 : -1;
        });
        
        foreach ($result as $account) {
            self::assertInstanceOf(AccountDto::class, $account);
        }
        self::assertEquals($sortedArray, $result);
    }
}
