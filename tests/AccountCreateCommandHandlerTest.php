<?php
declare(strict_types=1);

namespace App\Tests;

use App\Application\Command\AccountCreateCommand;
use App\Infrastructure\DTO\Entity\AccountDto;
use Throwable;

/**
 * Test the behaviour of the create account class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountCreateCommandHandlerTest extends AbstractTest
{
    private const OVERDRAFT                = 250;
    private const NOT_BLANK_MESSAGE        = 'This value should not be blank.';
    private const POSITIVE_OR_ZERO_MESSAGE = 'This value should be either positive or zero.';
    private const STRING_LENGTH_MESSAGE = 'This value is too long. It should have 120 characters or less.';
    
    public function testCreateAccountSuccessful()
    {
        $expectedFirstName = $this->faker->text(10);
        $expectedLastName  = $this->faker->text(14);
        $expectedBalance   = $this->faker->randomFloat();
        $result            = $this->commandBus->handle(
            new AccountCreateCommand(
                $expectedFirstName,
                $expectedLastName,
                $expectedBalance
            )
        );
        
        self::assertInstanceOf(AccountDto::class, $result);
        self::assertEquals($expectedFirstName, $result->getFirstName());
        self::assertEquals($expectedLastName, $result->getLastName());
        self::assertEquals($expectedBalance, $result->getBalance());
        self::assertEquals(self::OVERDRAFT, $result->getOverdraft());
    }
    
    public function testOnCreateAccountWithoutFirstName()
    {
        try {
            $this->commandBus->handle(
                new AccountCreateCommand(
                    '',
                    $this->faker->text(14),
                    $this->faker->randomFloat()
                )
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::NOT_BLANK_MESSAGE);
        }
    }
    
    public function testOnCreateAccountWithoutLastName()
    {
        try {
            $this->commandBus->handle(
                new AccountCreateCommand(
                    $this->faker->text(10),
                    '',
                    $this->faker->randomFloat()
                )
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::NOT_BLANK_MESSAGE);
        }
    }
    
    public function testOnCreateAccountWithNegativeBalance()
    {
        try {
            $this->commandBus->handle(
                new AccountCreateCommand(
                    $this->faker->text(10),
                    $this->faker->text(10),
                    -122
                )
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 1);
            $violation = $exception->getViolations()->offsetGet(0);
            self::assertEquals($violation->getMessage(), self::POSITIVE_OR_ZERO_MESSAGE);
        }
    }
    
    public function testOnCreateAccountWithMultipleViolation()
    {
        try {
            $this->commandBus->handle(
                new AccountCreateCommand(
                    '',
                    $this->faker->sentence(40),
                    -122
                )
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 3);
            foreach ($exception->getViolations() as $violation){
                self::assertTrue(
                    $violation->getMessage() === self::NOT_BLANK_MESSAGE ||
                    $violation->getMessage() === self::POSITIVE_OR_ZERO_MESSAGE ||
                    $violation->getMessage() === self::STRING_LENGTH_MESSAGE
                );
            }
            
        }
    }
}
