<?php
declare(strict_types=1);

namespace App\Tests;

use App\Application\Query\AccountTransactionListQuery;
use App\Infrastructure\DAL\AccountDal;
use App\Infrastructure\DAL\TransactionDal;
use App\Infrastructure\DTO\Entity\AccountDto;
use App\Infrastructure\DTO\Entity\TransactionDto;
use Throwable;

/**
 * Test the behaviour of the account transaction list query handler class.
 *
 * @package App\Tests
 * @author  Ciprian Căplescu <ciprianac@dreamlabs.ro>
 */
class AccountTransactionListQueryHandlerTest extends AbstractTest
{
    private const VALID_ULID_MESSAGE    = 'This is not a valid ULID.';
    private const STRING_LENGTH_MESSAGE = 'This value should have exactly 26 characters.';
    protected AccountDal $accountDal;
    protected TransactionDal $transactionDal;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->accountDal     = self::getContainer()->get(AccountDal::class);
        $this->transactionDal = self::getContainer()->get(TransactionDal::class);
    }
    
    protected function tearDown(): void
    {
        unset($this->accountDal);
        unset($this->transactionDal);
        parent::tearDown();
    }
    
    public function testAccountTransactionList()
    {
        $account1 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        
        $account2 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        $account3 = new AccountDto(
            $this->faker->text(10),
            $this->faker->text(14),
            $this->faker->randomFloat(),
            $this->faker->randomFloat()
        );
        $this->accountDal->persist($account1)
                         ->persist($account2)
                         ->persist($account3)
                         ->flush();
        
        $transaction1 = new TransactionDto($account1, $account2, $this->faker->randomFloat());
        $transaction2 = new TransactionDto($account2, $account3, $this->faker->randomFloat());
        $this->transactionDal->persist($transaction1)
                             ->persist($transaction2)
                             ->flush();
        
        $result = $this->queryBus->handle(new AccountTransactionListQuery($account2->getId()));
        
        self::assertIsArray($result);
        /** @var TransactionDto $transaction */
        foreach ($result as $transaction) {
            self::assertInstanceOf(TransactionDto::class, $transaction);
            self::assertTrue(
                $transaction->getSender()->getId() === $account2->getId() ||
                $transaction->getReceiver()->getId() === $account2->getId()
            );
        }
    }
    
    public function testWithInvalidUlid()
    {
        try {
            $this->queryBus->handle(
                new AccountTransactionListQuery($this->faker->text(10),)
            );
        } catch (Throwable $exception) {
            self::assertEquals($exception->getViolations()->count(), 4);
            foreach ($exception->getViolations() as $violation) {
                self::assertTrue(
                    $violation->getMessage() === self::VALID_ULID_MESSAGE ||
                    $violation->getMessage() === self::STRING_LENGTH_MESSAGE
                );
            }
        }
    }
}
